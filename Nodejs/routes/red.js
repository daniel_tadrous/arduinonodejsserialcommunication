var express = require('express');
var router = express.Router();

const SerialPort = require('serialport')
const port = new SerialPort('com3', {
  baudRate: 57600
})

router.get('/', function(req, res, next) {

  port.write(req.query['color']+req.query['mode'], function(err) {
    if (err) {
      return console.log('Error on write: ', err.message)
    }
    console.log('message written')
  })
  
  // Open errors will be emitted as an error event
  port.on('error', function(err) {
    console.log('Error: ', err.message)
  })

  // port.on('readable', function () {
  //   console.log('Data:', port.read())
  // })
  
  // // Switches the port into "flowing mode"
  // port.on('data', function (data) {
  //   console.log('Data:', data)
  // })
  //res.render('red', { title: 'Express' });
  res.render('index', { title: 'Saints lofi serial communication test' });

});

router.get('/', function(req, res, next) {
  
 

  port.write('2d', function(err) {
    if (err) {
      return console.log('Error on write: ', err.message)
    }
    console.log('message written')
  })
  
  // Open errors will be emitted as an error event
  port.on('error', function(err) {
    console.log('Error: ', err.message)
  })

  // port.on('readable', function () {
  //   console.log('Data:', port.read())
  // })
  
  // // Switches the port into "flowing mode"
  // port.on('data', function (data) {
  //   console.log('Data:', data)
  // })
  //res.render('red', { title: 'Express' });
  res.send('respond with a resource');

});

module.exports = router;
