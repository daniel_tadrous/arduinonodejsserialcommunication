// LOFI Brain firmware to communicate with LOFI Robot apps and Chrome Plugin
// USB + Bluetooth version
// Author: Maciej Wojnicki
// WWW.LOFIROBOT.COM
// 28.06.2018

int current_byte = 0;
int prev_byte = 0;

void setup() {
  Serial.begin(57600);  
  pinMode(10,OUTPUT);
  pinMode(9,OUTPUT);
  pinMode(6,OUTPUT);
}

void loop() {
  //receiving data from Chrome plugin
  receiving();
}


void receiving() {

  if (Serial.available() > 0) {
      current_byte = Serial.read();
      outputs_set();
      prev_byte = current_byte;
  }  
}


void outputs_set() {
  //output1
  if (prev_byte == 49) {
      all_stop();
      analogWrite(10,100);
  }
  //output2
    if (prev_byte == 50) {
      all_stop();
      analogWrite(9,100);
  }
  //output3
  if (prev_byte == 51) {
      all_stop();
      analogWrite(6,100);
  }
  
  if (prev_byte == 52 && current_byte == 52) {
    all_stop();
  }

}

void all_stop() {
  analogWrite(10, LOW);
  analogWrite(9, LOW);
  analogWrite(6, LOW);
}
